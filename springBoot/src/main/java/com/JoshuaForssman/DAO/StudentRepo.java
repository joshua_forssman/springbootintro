package com.JoshuaForssman.DAO;

import com.JoshuaForssman.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student, Integer>{
}
