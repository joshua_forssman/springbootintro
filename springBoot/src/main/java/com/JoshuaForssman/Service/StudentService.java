package com.JoshuaForssman.Service;

import com.JoshuaForssman.Entity.Student;

import java.util.List;

public interface StudentService {
     Student getStudentByID(int id);


     void removeStudentById(int id) ;
      void  updateStudent(Student stud);
   void insertStudent(Student stud) ;

    List<Student> getAllStudents();
}
