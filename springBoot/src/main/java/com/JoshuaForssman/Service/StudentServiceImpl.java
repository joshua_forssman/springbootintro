package com.JoshuaForssman.Service;

import com.JoshuaForssman.DAO.StudentRepo;
import com.JoshuaForssman.Entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {


    private StudentRepo studentRepo;

    public StudentServiceImpl(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public List<Student> getAllStudents(){

        return this.studentRepo.findAll();
    }

    public Student getStudentByID(int id){
        return this.studentRepo.getOne(id);
    }

    public void removeStudentById(int id) {
        Student student= getStudentByID(id);
        this.studentRepo.delete(student);
    }
    public  void  updateStudent(Student stud)
    {
        this.studentRepo.save(stud);
    }

    public void insertStudent(Student stud) {
        this.studentRepo.save(stud);
    }
}
